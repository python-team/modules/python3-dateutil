#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PY3VERS:=$(shell py3versions -r)

# Grab the version before +dfsg
DEB_DEBIAN_DIR=$(dir $(firstword $(MAKEFILE_LIST)))
DEB_UPSTREAM_VERSION=$(shell dpkg-parsechangelog -l$(DEB_DEBIAN_DIR)/changelog \
                               | sed -rne 's,^Version: ([^+]+).*,\1,p')

%:
	dh $@ --with python3

override_dh_auto_build:
	set -e; \
	for python in $(PY3VERS); do \
		$$python setup.py build; \
	done

override_dh_auto_install:
	set -e; \
	for python in $(PY3VERS); do \
		$$python setup.py install --install-layout=deb \
			--root=$(CURDIR)/debian//python3-dateutil; \
	done
	
	# The source should now be repacked without this file.
	#rm -f debian/python3-dateutil/usr/lib/python3/dist-packages/dateutil/zoneinfo/zoneinfo-*.tar.gz

override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	set -e; \
	for python in $(PY3VERS); do \
		$$python test.py; \
	done
endif

override_dh_clean:
	dh_clean
	rm -rf build
	rm -rf dateutil/__pycache__
	rm -rf dateutil/*/__pycache__


get-orig-source:
	uscan --noconf --force-download --rename --download-current-version \
			--check-dirname-level=0 --destdir=`pwd` $(DEB_DEBIAN_DIR)/..
	rm -rf python-dateutil-$(DEB_UPSTREAM_VERSION)
	tar -xf python3-dateutil_$(DEB_UPSTREAM_VERSION).orig.tar.gz
	rm python3-dateutil_$(DEB_UPSTREAM_VERSION).orig.tar.gz
	rm python-dateutil-$(DEB_UPSTREAM_VERSION)/dateutil/zoneinfo/zoneinfo-*.tar.gz
	mv python-dateutil-$(DEB_UPSTREAM_VERSION) python3-dateutil-$(DEB_UPSTREAM_VERSION).orig
	GZIP=--best tar -cz --owner root --group root --mode a+rX \
			-f python3-dateutil_$(DEB_UPSTREAM_VERSION)+dfsg1.orig.tar.gz \
			python3-dateutil-$(DEB_UPSTREAM_VERSION).orig
	rm -r python3-dateutil-$(DEB_UPSTREAM_VERSION).orig
