Source: python3-dateutil
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Thomas Kluyver <thomas@kluyver.me.uk>
Build-Depends: debhelper (>= 8), python3-all, python3-setuptools
Standards-Version: 3.9.3
Homepage: http://labix.org/python-dateutil
Vcs-Git: https://salsa.debian.org/python-team/packages/python3-dateutil.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python3-dateutil

Package: python3-dateutil
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, tzdata
Description: powerful extensions to the standard datetime module in Python 3
 The dateutil package extends the standard datetime module with:
 .
  * computing of relative deltas (next month, next year, next Monday, last week
    of month, etc);
  * computing of relative deltas between two given date and/or datetime objects
  * computing of dates based on very flexible recurrence rules, using a superset
    of the iCalendar specification. Parsing of RFC strings is supported as well.
  * generic parsing of dates in almost any string format
  * timezone (tzinfo) implementations for tzfile(5) format files
    (/etc/localtime, /usr/share/zoneinfo, etc), TZ environment string (in all
    known formats), iCalendar format files, given ranges (with help from
    relative deltas), local machine timezone, fixed offset timezone, UTC
    timezone
  * computing of Easter Sunday dates for any given year, using Western, Orthodox
    or Julian algorithms
